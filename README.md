# prl

`prl` aims to make typing long command faster.
For example instead of `systemctl` you can type just `sys`, as long as `sys` is not a executable in your path.
If you are familiar with cisco's shell, that's basically what I'm trying to achieve.

# Why

Why not just use aliases?
Unlike aliases you can type any part of the command name.
For example when typing `systemctl` you can type any part of the name (`sys`, `s` or `system`), as long as it's unique and it's not other executable in your PATH.

Other reason is that in future it will (hopefully) support shortening of arguments, where instead of typing `git remote add origin` you can type e.g. `gi r a origin`.

# Usage

To run it you can use:
`PATH="$PATH:__PATH_END__" LD_PRELOAD=./target/debug/libprl.so bash`

`__PATH_END__` needs to be last entry in `PATH`.
Shell tries every entry in `PATH` concatenated with what you just entered until it finds (or doesn't find) something.
`prl` hooks into `stat`(used to find executable) and `execve`(used to run the executable) calls made by your shell and when it sees call with path starting with `__PATH_END__` `prl` searches config for valid entry and if it finds matching entry, `stat` call returns 0 (found) and no entries in `PATH` after it are searched.

You also need a config in same directory (for now) named `cmd.yaml`, which contains command and path, for example:

```
cmds:
- short: 'systemctl'
  full: '/usr/bin/systemctl'
- short: 'git'
  full: '/usr/bin/git'
```
