/*
Copyright (C) 2023  Matěj Vrba

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use serde::Deserialize;
use serde::Serialize;
use std::ffi::CString;
use std::ffi::CStr;
use upm::Upm;
use std::sync::Mutex;
use std::fs;


use libc::c_char;
#[link_section = ".init_array"]
pub static INITIALIZE: extern "C" fn() = crate::rust_ctor;
type LibCStatT = fn(pathname: *const c_char, statbuf: *const libc::stat) -> i32;
type LibCExecveT = fn(pathname: *const c_char, argv: *const *const c_char, envp: *const *const c_char) -> i32;


static mut UPM_MAP: Mutex<Upm<Cmd>> = Mutex::new(Upm::<Cmd>::new());

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Cmd {
    short: String,
    full: String,
    args: Vec<Arg>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Arg {
    name: String,
    next: Vec<Arg>,
}
#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Cmds {
    cmds: Vec<Cmd>,
}


static PREFIX : &'static str = "__PATH_END__/";


#[no_mangle]
pub extern "C" fn stat(pathname: *const c_char, statbuf: *const libc::stat)-> libc::pid_t{
    // Convert c string to something rust likes
    let path = unsafe{
         CStr::from_ptr(pathname.clone()).to_str().unwrap()
    };
    // PREFIX is expected to be last entry in PATH, look for it
    if path.starts_with(PREFIX){

        // PREFIX was found - this means that entered executable name is not in PATH,
        // remove PREFIX and save the part we're looking for into separate var
        let target = String::from(path).split_off(PREFIX.len());

        unsafe{
            // rust doesn't like mutable globals -> mutex
            let ump = UPM_MAP.lock().unwrap();

            // if `target` can be found, return 0 (see `man stat` for return values)
            // NOTE: possible optimalization: save found `name` to global variable
            // and in `execve` instead of searching UPM first look into this variable
            if let Ok(_) = ump.find(target.as_str()){
                return 0;
            }else{
                // not found -> return -1
                return -1;
            }
        }
    }

    // use dlsym to get pointer to stat function, call it and return result
    let addr = unsafe{
         libc::dlsym(libc::RTLD_NEXT, CString::new("stat").unwrap().into_raw())
    };

    let real_stat: LibCStatT = unsafe { std::mem::transmute(addr) };
    return (real_stat)(pathname, statbuf);
}

#[no_mangle]
pub extern "C" fn execve(pathname: *const c_char, argv: *const *const c_char, envp: *const *const c_char)-> i32{
    let path = unsafe{
         CStr::from_ptr(pathname.clone()).to_str().unwrap()
    };

    let addr = unsafe{
         libc::dlsym(libc::RTLD_NEXT, CString::new("execve").unwrap().into_raw())
    };
    let real_execve: LibCExecveT = unsafe { std::mem::transmute(addr) };

    if path.starts_with(PREFIX){
        // PREFIX was found - this means that entered executable name is not in PATH,
        // remove PREFIX and save the part we're looking for into separate var
        let target = String::from(path).split_off(PREFIX.len());

            // rust doesn't like mutable globals -> mutex
        let ump = unsafe{
            UPM_MAP.lock().unwrap()
        };

            // if `target` is found, run it
            if let Ok(name) = ump.find(target.as_str()){
                print!("Expanded to: {}", name.short);
                let c_name = CString::new(name.full.as_str()).unwrap();
                let mut result_args :Vec<CString>= vec![];

                result_args.push(unsafe{
                        CStr::from_ptr(*(argv).clone()).into()
                    });
                //args
                let mut offset = 1;
                let mut args = Some(&name.args);

                //expand arguments
                while unsafe{*argv.offset(offset)} != std::ptr::null() && offset != -1{
                    // get next argument
                    let arg = unsafe{
                        CStr::from_ptr(*(argv.offset(offset)).clone()).to_str().unwrap()
                    };

                    // fill upm with possible arguments
                    if let Some(args_some) = args{
                    let mut upm = Upm::<String>::new();
                    for arg in args_some{
                        upm.insert(arg.name.as_str(), arg.name.clone()).unwrap();
                    }

                    // try to find an argument
                    match upm.find(arg){
                        Ok(val) =>{
                            print!(" {}", val);
                            result_args.push(CString::new(val.clone()).unwrap());
                            for arg in args_some{
                                if arg.name == *val{
                                    args = Some(&arg.next);
                                }
                            }
                        }
                        Err(_) => {
                            args = None;
                            result_args.push(CString::new(arg).unwrap());
                            print!(" {}", arg);
                        }
                    }
                    }else{
                            result_args.push(CString::new(arg).unwrap());
                            print!(" {}", arg);
                    }

                    /*
                    for cmd in args{
                        let mut found = false;
                        if cmd.name.starts_with(arg){
                            found = true;
                            println!("match {} {}", arg, cmd.name);
                            args = &cmd.next;
                        }
                    }
                    */

                    offset += 1;
                }
                println!("");


                // Convert list of arguments to c array of strings
                let mut ptrs: Vec<*const c_char> = result_args.iter().map(|s| s.as_ptr()).collect();
                ptrs.push(std::ptr::null()); // add a null terminator to the end of the array
                let array = ptrs.as_ptr();
                std::mem::forget(ptrs); // prevent the vector from being deallocated

                return (real_execve)(c_name.as_ptr(), array, envp);
            }else{
                // not found -> return -1
                println!("Not found");
                todo!();
            }
    }else{

    }

    return (real_execve)(pathname, argv, envp);
}

fn load(file: String) -> Result<Cmds, std::io::Error> {
    let file = fs::read_to_string(file)?;
    let cmds: Cmds = serde_yaml::from_str(file.as_str()).unwrap();
    Ok(cmds)
}

#[no_mangle]
pub extern "C" fn rust_ctor() {
    let config: String = if cfg!(debug_assertions) {
        String::from("./cmd.yaml")
    } else {
        todo!();
        //format!("{}/...", env::var("HOME").unwrap())
    };

    let cmds = if let Ok(cmds) = load(config){
        cmds
    }else{
        Cmds{ cmds: vec![] }
    };


    for c in cmds.cmds{
    unsafe{
    //    eprintln!("added {} -> {}", c.short, c.full);
        UPM_MAP.get_mut().unwrap().insert(c.short.clone().as_str(), c).unwrap();
    }

    }

}
